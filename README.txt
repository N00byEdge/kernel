    How to compile?

Try the example program:
./compile.py --objs c examples/program.c build/kernel.elf
./mkbootable.sh build/kernel.elf build/bootable.img

And then try booting in qemu:
qemu-system-x86_64 -hda build/bootable.img

Or do it all in a row:
./test.sh
