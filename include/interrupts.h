#include <stdint.h>
#include <stddef.h>

#define IDT_SIZE 256
#define INTERRUPT_GATE 0x8e
#define KERNEL_CODE_SEGMENT_OFFSET 0x08

#define throw(num) asm volatile("int $"#num);

struct idt_entry_t {
    uint16_t offset_lo;
    uint16_t selector;
    uint8_t ist;
    uint8_t type_attr;
    uint16_t offset_mid;
    uint32_t offset_hi;
    uint32_t zero;
} __attribute__((packed));

void idt_init(void);
int register_interrupt_handler(size_t vec, void *handler, uint8_t ist, uint8_t type);
