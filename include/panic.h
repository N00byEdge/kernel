_Noreturn void panic(const char *message);
_Noreturn void cpu_exception(void *);
#define PANIC_IF(expr) if (expr) panic("("#expr") == true")
