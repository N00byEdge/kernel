#pragma once
#include <stdint.h>
#include "stivale_structs.h"

extern uint64_t cmdline_addr;
extern struct mmap_entry *mmap;
extern uint64_t mmap_entries;
extern struct mmap_entry higherhalf;
extern struct stivale_struct* stivale;

void init_from_stivale(struct stivale_struct* strct);
