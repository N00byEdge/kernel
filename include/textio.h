#include <stdbool.h>
#include <stdint.h>

#define print_hex(num, padding) print_hex_impl((num), sizeof((num)) * 2, padding)

extern uint8_t font_padding;
extern struct pixel32 font_color_fg;
extern struct pixel32 font_color_bg;
extern struct coords textio_cursor;

void textio_init();
bool putchar_simple(const char character);
void putchar(const char character);
void print(const char *string);
void print_hex_impl(uint64_t num, int nibbles, bool padding);
void print_num(uint64_t num, bool padding);

enum pixstate {
    aval_free,
    aval_from_out,
    aval_from_in
};
