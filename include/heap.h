#include <stdbool.h>


struct memchunk_free {
    uint64_t size;
    struct memchunk_free *next;
};


void heap_init();
void *heap_alloc(uint64_t size);
void *heap_dealloc(void *memptr);
