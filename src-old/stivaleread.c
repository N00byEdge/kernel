#include <stdint.h>
#include <stddef.h>
#include "stivale.h"

struct mmap_entry higherhalf;
uint64_t cmdline_addr;
struct mmap_entry *mmap;
uint64_t mmap_entries;

void init_from_stivale(struct stivale_struct* strct) {
    cmdline_addr = strct->cmdline;
    mmap = (struct mmap_entry *)strct->memory_map_addr;
    mmap_entries = strct->memory_map_entries;
    for (size_t it = 0; it < mmap_entries; it++) {
        if (mmap[it].type == 1)
            higherhalf = mmap[it];
    }
}
