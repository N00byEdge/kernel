#define LINES 25
#define COLUMNS 80
#define BYTES_FOR_EACH_ELEMENT 2
#define SCREENSIZE BYTES_FOR_EACH_ELEMENT * COLUMNS * LINES

#define TEXT_COLOR 0x07   // FG: light grey  BG: black
#define CURSOR_COLOR 0x04 // FG: red         BG: black
