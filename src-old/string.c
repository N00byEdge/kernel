#include <stddef.h>
#include <stdint.h>
#include "helpers.h"
#include "string.h"


size_t strlen(const char *s) {
    for (size_t it = 0; 1; it++) {
        if (s[it] == 0) {
            return it;
        }
    }
}

void* memset(void *dst, int ch, size_t count) {
    for(size_t i = 0; i < count; i++)
      ((char*)dst)[i] = ch;
    return dst;
}

void* memcpy(void *dst, const void *src, size_t len) {
  for(size_t i = 0; i < len; i++)
    ((uint8_t*)dst)[i] = ((uint8_t*)src)[i];
  return dst;
}

char *strcpy(char *dest, const char *src) {
    memcpy(dest, src, strlen(src) + 1);
    return dest;
}

char *strncpy(char *dest, const char *src, size_t n) {
    size_t it;
    for (it = 0; it < n && src[it]; it++)
        dest[it] = src[it];
    for (; it < n; it++)
        dest[it] = 0;
    return dest;
}

char *strcat(char *dest, const char *src) {
    strncpy(dest + strlen(dest), src, strlen(src));
    return dest;
}

unsigned int strstartswith(const char *base, const char *str) {
    for (size_t it = 0; str[it] != 0; it++) {
        if (base[it] != str[it]) {
            return 0;
        }
    }
    return 1;
}

unsigned int strrepl(char *str, unsigned int times, char what, char to) {
    unsigned int occurencies = 0;
    for(size_t it = 0; it <= strlen(str); it++) {
        if(str[it] == what) {
            str[it] = to;
            occurencies++;
            if (occurencies != 0 && occurencies == times)
                break;
        }
    }
    return occurencies;
}
