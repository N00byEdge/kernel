#include "simplecore.h"
#include "keyboard.h"
#include "kbvars.h"


unsigned int yesno(const char *message) {
    // Backup and disable keyboard echo state
    unsigned int kb_echo_bak = kb_echo;
    kb_echo = 0;
    // Initialise variables
    int cbuffer = 0;
    unsigned int res;
    // Print the message and Check input
    sprint(message);
    while (1) {
        cbuffer = getch();
        if (cbuffer == 'y') {
            sprint("Yes");
            res = 1;
            break;
        } else if (cbuffer == 'n') {
            sprint("No");
            res = 0;
            break;
        }
    }
    // Print newline
    sprint_char('\n');
    // Restore keyboard echo state
    kb_echo = kb_echo_bak;
    return res;
}

int nonegnum(int num) {
    if (num < 0) {
        return 0;
    } else {
        return num;
    }
}
