#define BUF_SIZES 1024
typedef unsigned int mini2dpixel;

struct coords_real {
    int x, y;
};
struct coords_2d {
    float x, y;
};
struct line {
    struct coords_2d start;
    struct coords_2d end;
    mini2dpixel color;
};
struct rectangle {
    struct coords_2d topleft;
    struct coords_2d bottomright;
    mini2dpixel color;
};
struct mini2d {
    int screen_width;
    int screen_height;
    mini2dpixel *screen_begin;
    mini2dpixel *screen_half;
    mini2dpixel *screen_end;
};

struct mini2d mini2d(mini2dpixel *buffer, int width, int height);
int mini2d_drawpix(struct mini2d *state, struct coords_real *coords, mini2dpixel color);
void mini2d_reldrawpix(struct mini2d *state, struct coords_2d *coords, mini2dpixel color);
void mini2d_drawline(struct mini2d *state, struct line *thisline);
void mini2d_drawrect(struct mini2d *state, struct rectangle *thisrect);
