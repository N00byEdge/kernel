extern void load_idt(unsigned long *idt_ptr);
extern char port_inb(unsigned short port);
extern void port_outb(unsigned short port, unsigned char data);
extern void disable_cursor(void);
