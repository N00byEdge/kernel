#include <stddef.h>
#include "simplecore.h"
#include "keyboard.h"
#include "helpers.h"
#include "string.h"
#include "types.h"
#include "stivale.h"
#include "stdlib.h"

#define MAX_ALLOCS 256// Max. 256 allocations possible


char* itoa(int value, char* result, int base) {
    /**
    * C++ version 0.4 char* style "itoa":
    * Written by Lukás Chmela
    * Released under GPLv3.

    */
    // check that the base if valid
    if (base < 2 || base > 36) { *result = '\0'; return result; }

    char* ptr = result, *ptr1 = result, tmp_char;
    int tmp_value;

    do {
    tmp_value = value;
        value /= base;
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
    } while ( value );

    // Apply negative sign
    if (tmp_value < 0) *ptr++ = '-';
    *ptr-- = '\0';
    while(ptr1 < ptr) {
        tmp_char = *ptr;
        *ptr--= *ptr1;
        *ptr1++ = tmp_char;
    }
    return result;
}

int atoi(const char *str) {
    int res = 0;
    size_t thisstep = 1;
    // Iterate though each character
    for (size_t it = strlen(str) - 1; it != -1; it--) {
        if (str[it] == '-' && it == 0) {
            res -= res * 2;
            continue;
        }
        res += (str[it] - 48) * thisstep;
        thisstep *= 10;
    }
    return res;
}
