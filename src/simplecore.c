#include "stivale.h"
#include "panic.h"
#include "textio.h"
#include "stdio.h"
#include "interrupts.h"
#include "keyboard.h"
#include "heap.h"
#include "string.h"


_Noreturn void kmain(struct stivale_struct* strct) {
    // Set doublefault handler
    //register_interrupt_handler(0x08, cpu_exception, 0, INTERRUPT_GATE);
    // Initiliase environemt
    init_from_stivale(strct);
    heap_init();
    textio_init();
    kb_init();
    idt_init();
    kb_enable();
    // DEBUG
    print("struct stivale_struct stivale = {\n"); {
        #define STIVALE_DEBUG(key, type) printf("    ."#key" = %"#type";\n", stivale->key);
        STIVALE_DEBUG(cmdline, x);
        STIVALE_DEBUG(memory_map_addr, x);
        STIVALE_DEBUG(memory_map_entries, l);
        STIVALE_DEBUG(framebuffer_addr, x);
        STIVALE_DEBUG(framebuffer_pitch, l);
        STIVALE_DEBUG(framebuffer_width, l);
        STIVALE_DEBUG(framebuffer_height, l);
        STIVALE_DEBUG(framebuffer_bpp, l);
        STIVALE_DEBUG(rsdp, x);
        STIVALE_DEBUG(module_count, l);
        STIVALE_DEBUG(modules, x);
        STIVALE_DEBUG(epoch, l);
    } print("};\n");
    print("Running heap tests...\n");
    const char demostring1[] = "Hello world!\n";
    const char demostring2[] = "Test...\n";
    print(demostring1);
    char *test1 = heap_alloc(sizeof(demostring1));
    char *test2 = heap_alloc(sizeof(demostring1));
    char *test3 = heap_alloc(sizeof(demostring1));
    char *test4 = heap_alloc(sizeof(demostring1));
    strcpy(test1, demostring1);
    strcpy(test2, demostring1);
    strcpy(test3, demostring1);
    strcpy(test4, demostring1);
    print(test1);
    print(test2);
    print(test3);
    print(test4);
    heap_dealloc(test2);
    test2 = heap_alloc(sizeof(demostring2));
    strcpy(test2, demostring2);
    print(test1);
    print(test2);
    print(test3);
    print(test4);
    if (test2 < test4) {
        print("Success!!! :D\n");
    } else {
        print("Failure :-(\n");
    }
    while(1);
}
