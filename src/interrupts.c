#include <stdint.h>
#include <stddef.h>
#include "asmfn.h"
#include "interrupts.h"


struct idt_entry_t idt[IDT_SIZE];


int register_interrupt_handler(size_t vec, void *handler, uint8_t ist, uint8_t type) {
    uint64_t p = (uint64_t)handler;

    idt[vec].offset_lo = (uint16_t)p;
    idt[vec].selector = 0x08;
    idt[vec].ist = ist;
    idt[vec].type_attr = type;
    idt[vec].offset_mid = (uint16_t)(p >> 16);
    idt[vec].offset_hi = (uint32_t)(p >> 32);
    idt[vec].zero = 0;

    return 0;
}

void idt_init(void) {
        size_t keyboard_address;
        unsigned long idt_address;
        struct {
          uint16_t limit;
          uint64_t addr;
        } __attribute__((packed)) idt_ptr;

        /*     Ports
        *	 PIC1	PIC2
        *Command 0x20	0xA0
        *Data	 0x21	0xA1
        */

        /* ICW1 - begin initialization */
        port_outb(0x20 , 0x11);
        port_outb(0xA0 , 0x11);

        /* ICW2 - remap offset address of IDT */
        /*
        * In x86 protected mode, we have to remap the PICs beyond 0x20 because
        * Intel have designated the first 32 interrupts as "reserved" for cpu exceptions
        */
        port_outb(0x21 , 0x20);
        port_outb(0xA1 , 0x28);

        /* ICW3 - setup cascading */
        port_outb(0x21 , 0x00);
        port_outb(0xA1 , 0x00);

        /* ICW4 - environment info */
        port_outb(0x21 , 0x01);
        port_outb(0xA1 , 0x01);
        /* Initialization finished */

        /* mask interrupts */
        port_outb(0x21 , 0xff);
        port_outb(0xA1 , 0xff);

        /* fill the IDT descriptor */
        idt_address = (unsigned long)idt;
        idt_ptr.limit = IDT_SIZE * sizeof(struct idt_entry_t) - 1;
        idt_ptr.addr = (uint64_t)idt;

        load_idt(&idt_ptr);
}
