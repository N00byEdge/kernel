#include <stdarg.h>
#include <stdbool.h>
#include <limits.h>
#include "string.h"
#include "textio.h"


void print_hex_impl(uint64_t num, int nibbles, bool padding) {
    // Check if number is 0
    if (num == 0 && !padding) {
        putchar('0');
        return;
    }
    // Start printing
    bool ispadding = !padding;
    for(int i = nibbles - 1; i >= 0; -- i) {
        char out = "0123456789ABCDEF"[(num >> (i * 4))&0xF];
        // If number isn't 0, this isn't padding
        if (out != '0') {
            ispadding = false;
        }
        // If no more padding, print
        if (!ispadding) {
            putchar(out);
        }
    }
}

void print_num(uint64_t num, bool padding) {
    // Check if number is 0
    if (num == 0 && !padding) {
        putchar('0');
        return;
    }
    // Start printing
    bool ispadding = !padding;
    for (int it = 60; it >= 0; it -= 4) {
        char out = (num >> it) & 0xF;
        out += (out < 10) ? '0' : ('A' - 10);
        // If number isn't 0, this isn't padding
        if (out != '0') {
            ispadding = false;
        }
        // If no more padding, print
        if (!ispadding) {
            putchar(out);
        }
    }
}

int printf(const char *format, ...) {
    va_list ap;
    bool nexteval = false;
    va_start(ap, format);
    for (const char *thischar = format; *thischar != '\0'; thischar++) {
        if (*thischar == '%') {
            if (!(nexteval = !nexteval)) {
                putchar('%');
            }
        } else if (nexteval) {
            switch(*thischar) {
                case 's': print(va_arg(ap, const char *)); break;
                case 'c': putchar((const char)va_arg(ap, const int)); break;
                case 'x': print("0x"); case 'p': print_hex(va_arg(ap, uint64_t), true); break;
                case 'd': case 'i': case 'l': print_num(va_arg(ap, uint64_t), false); break;
                default: print("<unknown type>");
            }
            nexteval = false;
        } else {
            putchar(*thischar);
        }
    }
    va_end(ap);
}
